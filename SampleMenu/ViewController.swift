//
//  ViewController.swift
//  SampleMenu
//
//  Created by Developer on 12/11/15.
//  Copyright © 2015 Cycle. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    lazy var menuTD = MenuTD()

    @IBAction func menu(sender: AnyObject) {
        guard let vc = storyboard?.instantiateViewControllerWithIdentifier("menu") else {
            return
        }
        vc.transitioningDelegate = self.menuTD
        vc.modalPresentationStyle = .Custom
        presentViewController(vc, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

