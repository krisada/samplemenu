//
//  MenuPC.swift
//  RetailStore
//
//  Created by Krisada Kaewjunchai on 11/26/2557 BE.
//  Copyright (c) 2557 Krisada.K. All rights reserved.
//

import UIKit

class MenuPC: UIPresentationController {
    
    let dimmingView = UIView()
    var tapGesture:UITapGestureRecognizer!
    
    override init(presentedViewController: UIViewController, presentingViewController: UIViewController) {
        super.init(presentedViewController: presentedViewController, presentingViewController: presentingViewController)
        dimmingView.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        self.tapGesture = UITapGestureRecognizer(target: self, action: "onTapOutSide:")
        self.dimmingView.addGestureRecognizer(self.tapGesture)
    }
    
    func onTapOutSide(sender:UITapGestureRecognizer) {
        presentingViewController.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func presentationTransitionWillBegin() {
        dimmingView.frame = containerView!.bounds
        dimmingView.alpha = 0
        presentedViewController.view.alpha = 0
        containerView!.insertSubview(dimmingView, atIndex: 0)
        
        presentedViewController.transitionCoordinator()?.animateAlongsideTransition({ context in
            self.presentedViewController.view.alpha = 1
            self.presentedViewController.view.layer.shadowColor = UIColor.blackColor().CGColor
            self.presentedViewController.view.layer.shadowOffset = CGSizeMake(12, 12)
            self.presentedViewController.view.layer.shadowRadius = 12
            self.presentedViewController.view.layer.shadowOpacity = 0.5
            self.dimmingView.alpha = 1.0
            }, completion: nil)
    }
    
    override func dismissalTransitionWillBegin() {
        presentedViewController.transitionCoordinator()?.animateAlongsideTransition({context in
            self.dimmingView.alpha = 0.0
            }, completion: nil)
    }
    
    override func frameOfPresentedViewInContainerView() -> CGRect {
        
        let menuFrame = CGRectMake(0, 0, containerView!.bounds.width - 40, containerView!.bounds.height)
        return menuFrame
    }
    
    deinit {
        self.dimmingView.removeFromSuperview()
    }
}
